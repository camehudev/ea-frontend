import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.tsx'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import Home from './pages/Home.tsx'
import Usuarios from './pages/Usuarios.tsx'
import Nubank from './pages/bancos/Nubank.tsx'
import Erro_400 from './pages/error/Erro_400.tsx'
import Next from './pages/bancos/Next.tsx'
import Caixa from './pages/bancos/Caixa.tsx'
import Uploadpage from './pages/Upload/Uploadpage.tsx'

import { SignContextProvider } from './hooks/useSignContext.tsx'
import Login from './pages/Login/Login.tsx'


const router = createBrowserRouter([
  {path:'/',
  element:<App />,
  errorElement: <Erro_400/>,
  children:[
    
    {path:'/home',element:<Home />},
    {path:'/usuarios',element:<Usuarios />},
    {path:'/nubank',element:<Nubank />},
    {path:'/next',element:<Next />},
    {path:'/caixa',element:<Caixa />},
    {path:'/upload',element:<Uploadpage />},
    {path:'/login', element:<Login/>}
     
  ] },  
])

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>  
    
    <SignContextProvider>
      <RouterProvider router={router}/>
    </SignContextProvider>   
         
  </React.StrictMode>,
)
