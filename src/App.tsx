import { useContext} from "react"
import MasterPage from "./pages/MasterPage/MasterPage"
import Login from "./pages/Login/Login"
import { SignContext } from "./context/SignContext"


function App() {
  const user = useContext(SignContext)
 
  return (
    <>    
    {user != null?<MasterPage/>:<Login/>}     

    </>
  )
}

export default App
