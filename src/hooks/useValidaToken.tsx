import axios from "axios"
import { useState } from "react"
// import { SignContext } from "../context/SignContext"

export default async function ValidaToken(token:string | null){
  // const {setUser} = useContext<string | any | null>(SignContext)
  const [tokenUser, setTokenUser] = useState(localStorage.getItem('token'))
  setTokenUser(token)

  const response = await axios.post('http://localhost:8000/api/v1/validaToken',{idToken:tokenUser}).then(
        res =>res.data     
      ).catch(error=>{console.log(error.data)}
    )
   // setUser(response)
    
    return response
    
}

