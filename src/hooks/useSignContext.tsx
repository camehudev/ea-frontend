import { useState } from "react"
import { SignContext } from "../context/SignContext"


export const SignContextProvider=({children}:any | string | null | React.Dispatch<any>)=>{
  const [user] = useState<string | any| null>(localStorage.getItem('nickname'))  
  return(
    
    <SignContext.Provider value={user}>
      {children}
    </SignContext.Provider>
  )
}