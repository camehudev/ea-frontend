import NavBar from "../../componentes/header/NavBar"
import SideBar from "../../componentes/header/SideBar/SideBar"
import { Outlet } from "react-router-dom"

const MasterPage = () => {
  
  return (
    <>
    <NavBar/>
      <div className="d-flex justify-center justify-items-center">
       <Outlet/> 
      </div>
                
    <SideBar/>
    </>      
   
  )
}

export default MasterPage

