import styles from './next.module.css'

const Next = () => {
  return (
    <div className={`${styles.next_pag}`}><h1>Next</h1></div>
  )
}

export default Next