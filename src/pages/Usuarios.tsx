
import ModalAdd from '../componentes/ModalAdd'
import Table from '../componentes/Table'
import style from './usuarios.module.css'

const Usuarios = () => {
  return (
    <div className={`${style.usuario_cadastrar}`}>       
     <div>
        <ModalAdd/>       
       <div className='mt-4'> <Table/> </div>       
     </div>   
    </div>
  )
}

export default Usuarios