import { useContext } from 'react'
import style from './home.module.css'
import { SignContext } from '../context/SignContext'
import Login from './Login/Login'

const Home = () => {
  const user = useContext(SignContext)
  return (
    <>
        {user != null?    
          <div className={style.home_pag}>
          <div className="card bg-primary text-white">
          <div className="card-body">Primary card</div>
          </div>

            <div className="card bg-success text-white">
              <div className="card-body">Success card</div>
            </div>

            <div className="card bg-info text-white">
              <div className="card-body">{user}</div>
            </div>

        </div>
          
        : <Login/> }

    </>
  )  
}

export default Home