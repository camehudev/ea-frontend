import { useContext } from 'react'
import FromLogin from '../Login/FromLogin'
import styles from '../Login/login.module.css'
import { SignContext } from '../../context/SignContext'
import { Navigate} from 'react-router-dom'




const Login = () => {
   const user = useContext(SignContext)
   
  return (
    <> 
    {user === null?(   
          <div className={`${styles.login_ea}`}>       
                 <FromLogin/>
          </div>):<Navigate to="/home" replace={true} />}      
          
    </>           
       
  )
}

export default Login