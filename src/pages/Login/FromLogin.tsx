import axios from "axios";
import { useState } from "react";
import Swal from "sweetalert2";
import styles from '../Login/fromLogin.module.css'

const FromLogin =()=>{
  const [authNickName, setAuthNickName]=useState<string>('');
  const [authPassword, setAuthPassword]=useState<string>('');
  const[loading,setLoading]=useState(false)
  const [userError, setUserError] = useState<true | false>(false)
  const [passwordError, setPasswordError] = useState<true | false>(false)
  const [nickNameEmpyt, setNickNameEmpyt] = useState<true | false>(false)
  const [passWordEmpyt, setPassWordEmpyt] = useState<true | false>(false)


  function validaInput(): true | undefined {
    if(authNickName === null || authNickName === ''){
              setPasswordError(false)
              setUserError(false)
              setNickNameEmpyt(true) 
              setPassWordEmpyt(false)         
    }else{ if(authPassword === null || authPassword === ''){
                  setPasswordError(false)
                  setUserError(false)
                  setNickNameEmpyt(false)      
                  setPassWordEmpyt(true)              
            }else{
              setNickNameEmpyt(false) 
              setPassWordEmpyt(false) 
              return true
            }
    }}

    //  Criar uma requisição assincrona
   const validaLoginUsuario = async()=>{
    setLoading(true)
    try{
      const response =  await axios.post('http://localhost:8000/api/v1/user/auth/login',{
                nickName: authNickName,
                senhaUser: authPassword
              }).then(
                 res =>res.data).catch((error)=>{
                 if(error.message === 'Network Error'){                    
                    Swal.fire({
                      icon: 'error',
                      title: 'Oops...',
                      text: 'Falha ao se conectar ao servidor',
                      showConfirmButton: false,
                      timer: 2000
                      
                    }) 
                    
                    setAuthNickName('')
                    setAuthPassword('')                   

                    
                  }})

                setLoading(false)
                

            if (response[0] === 'Usuário autenticado com sucesso!'){
                            Swal.fire({
                              position: 'center',
                              title: response[0],
                              icon: 'success',                
                              showConfirmButton: false,
                              timer: 1500
                            })

                              localStorage.setItem('token',response[1])
                              localStorage.setItem('nickname', response[2])
                              setAuthNickName('');
                              setAuthPassword(''); 
                                                                             

                            
                          }else{                           
                                
                              if (response === 'Usuário inválido!'){
                                                  Swal.fire({
                                                    icon: 'error',
                                                    title: 'Oops...',
                                                    text: response,
                                                    showConfirmButton: false,
                                                    timer: 2000
                                                    
                                                  })

                                  setAuthNickName('')
                                  setAuthPassword('')
                                                
                                      }}

              if (response === 'Senha inválida!'){
                              Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: response,
                                showConfirmButton: false,
                                timer: 2000
                                
                              })
                              setAuthNickName('')
                              setAuthPassword('')}
        
    }catch{()=>{
      setLoading(false);
      alert('Erro ao se conectar ao servidor')
      
    }}
  }
  
  function handleSubmit(e:any) {
      e.preventDefault()
      if(validaInput()){
        setNickNameEmpyt(false)
        setPassWordEmpyt(false) 
        validaLoginUsuario()        
      }      
    }
 
    return (
   
    <div className="bg-light ">
        <div className="container-fluid bg-light">
                <img
                        id="ImgLogo"
                        src="/logo.svg"
                        alt="ImgLogo"
                        width={200}                                
                        className="img-fluid bg-light"
                    />
        </div>

        <form onSubmit={handleSubmit} className="p-3 bg-light">
          
            <div className="mb-3 mt-1 bg-light">
            <label htmlFor="text" className="form-label text-success bg-light">Email:</label>
            <input type="text" className="form-control text-secondary" value={authNickName}  placeholder="digite um email" name="user" onChange={(e)=>{setAuthNickName(e.target.value)}} required={true}/>
            {userError&&<small className={`${styles.error_message__login}`}>Usuario Invalido </small>}
            {nickNameEmpyt&&<small className={`${styles.error_message__login}`}>Campo Usuario deve ser preenchido </small>}                   
            </div>
                    

            <div className="mb-3 mt-2 bg-light">
                <label htmlFor="password" className="form-label text-success bg-light">Senha:</label>
                <input type="password" value={authPassword} className="form-control text-secondary" placeholder="digite uma senha" name="password" onChange={(e)=>{setAuthPassword(e.target.value)}}  required={true}/>
                 {passwordError&&<small className={`${styles.error_message__login}`}>Senha invalida</small>}
                 {passWordEmpyt&&<small className={`${styles.error_message__login}`}>Campo senha deve ser preenchido </small>}

                 
            </div>
           <div>
            <button type="submit" className="btn btn-success w-100 p-2" >ENTRAR</button>
            </div>
            {loading&&<div className="text-center mt-1 fs-4 text-success"><small>Carregando dados...</small></div>}
        </form>
               
  </div>
 
    )
 
}

export default FromLogin