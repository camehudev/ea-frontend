import ModalAdd from '../../../componentes/ModalAdd'
import style from '../usuarios/cadastrarUsuario.module.css'


const CadastrarUsuario = () => {   
  return (
    <>
        <div className={`${style.container}`}>
            <ModalAdd/>
        </div>       
       
    </>
    
  )
}

export default CadastrarUsuario