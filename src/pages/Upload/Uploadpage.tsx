import Upload from "../../componentes/Upload"
import style from './uploadpage.module.css'

const Uploadpage = () => {
  return (
    <div className={`${style.upload_pag}`}>
      <div className={`${style.upload_pag_input}`}>
            <Upload/>
      </div>    
      
    </div>
  )
}

export default Uploadpage