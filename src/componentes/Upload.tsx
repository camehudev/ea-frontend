import {useEffect, useState} from 'react'
import axios from 'axios'
import Swal from 'sweetalert2'
import '../pages/Upload/uploadpage.module.css'

const Upload = () => {
  
 const [arquivoExcell, setArquivoExcell] = useState<string >('')
 const [bancoUsado, setBancoUsado] = useState('')
 const [loading, setLoading] = useState(false)
 

  const postSalvarArquivoExcel = async()=>{  
   try {  
    setLoading(true)
    // alert(arquivoExcell)
    const response =  await axios.post('http://localhost:8000/api/v1/upload',{nomeArquivo:arquivoExcell, bancoUsado:bancoUsado}).then(
        res =>res.data       
      ).catch(error=>{    
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: error.message,
          showConfirmButton: false,
          timer: 2500          
        })  
        setArquivoExcell('')
      })       
        
      setLoading(false)
      

      if(response[0] === 'Itens salvos com sucesso'){
            Swal.fire({
              position: 'center',
              icon: 'success',
              title: response[0],
              showConfirmButton: false,
              timer: 1500
            })

          setArquivoExcell('')
      }
      
    } catch (error) {
      console.log(error)
             
      
    } };

  // Valida a extenção do arquivo
  const validaExtensao = ()=>{
      let extensao = arquivoExcell
      let pattern=/.xlsx/
      let patternTwo = /.xls/     
     
      if(pattern.test(extensao) === true || patternTwo.test(extensao)){
              return true
      }else{
        return false
      }
  }

   
  // Esta função pega o nome do arquivo
  function handleNameSubmit(e:any) {
    e.preventDefault()
    postSalvarArquivoExcel()
        
  }

  
  function handleErroXlsx(e:any) {
    e.preventDefault(); 
    setArquivoExcell('')   
  }


 // Esta função vai enviar os arquivo XLS para a API do banco de dados
    // será necesario usar a função GET
    useEffect(() => {  
    }, [])

    
 
  return (
 <>
    <div >
      <div>
        <form onSubmit={handleNameSubmit}>
          <label className='mt-3' htmlFor="myfile" >SELECIONE UM ARQUIVO:</label> 
          <div>
            <input className='form-control form-control-lg' type="file" id="myfile" name="arquivoExcell" value={arquivoExcell}  onChange={(e)=>{setArquivoExcell(e.target.value)}} required />
            <span className="glyphicon glyphicon-remove form-control-feedback"></span>
          </div>

          {validaExtensao() === false && arquivoExcell != ''?
                      ( <span>
                        <div className="alert alert-danger alert-dismissible mt-3">
                          <button type="submit" className="btn-close" data-bs-dismiss="alert" onClick={handleErroXlsx} ></button>
                          <strong>Atenção!</strong> Arquivo invalido. Formato suportado .xlsx ou xls.             
                         </div>            
                      </span>):  
                      
                      arquivoExcell != "" &&                        
                         (<span><select className="form-select mt-3"  onChange={(e)=>{setBancoUsado(e.target.value)}} placeholder='banco' required>
                         <option value={''} ></option>
                          <option value={'caixa'}>Caixa Econômica</option>
                          <option value={'next'}>Next</option>
                          <option value={'nubank'}>Nubank</option>
                         </select></span>)              
                }   


                {bancoUsado != "" && arquivoExcell != ''?
                          <button type="button" className="btn btn-secondary mt-2" onClick={handleNameSubmit} >ENVIAR</button>
                        :('')}   
        </form>  
                     
      </div>
 </div>
  <div className="container mt-2 text-center text-green-600 font-extralight">
     {loading?(<span className='loading'> Carregando dados ...</span>): ('')} 
 </div>
 </>

  

 
)
  }


export default Upload