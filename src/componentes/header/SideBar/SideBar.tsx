
import {NavLink } from 'react-router-dom'
import style from './sideBar.module.css'
import { useContext } from 'react'
import { SignContext } from '../../../context/SignContext'
import Login from '../../../pages/Login/Login'


const SideBar = () => {
  const user =useContext(SignContext)
  return (
    
    <div className={`${style.sidebar}`}>
      <div className={`${style.Nav_link}`}></div>
      {user != ''?< NavLink className="nav-link"  to={'/next'}>Next</NavLink>:<Login/>}
      {user != ''? < NavLink className="nav-link"  to={'/Nubank'}>Nubank</NavLink>:<Login/>}
      {user != ''?< NavLink className="nav-link"  to={'/caixa'}>Caixa</NavLink>:<Login/>}
      {user != ''?< NavLink className="nav-link"  to={'/upload'}>Upload</NavLink>:<Login/>}
       
    </div>

  
  )
}

export default SideBar