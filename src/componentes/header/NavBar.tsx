import { useContext } from 'react'
import {NavLink} from 'react-router-dom'
import { SignContext } from '../../context/SignContext'
import Login from '../../pages/Login/Login'


const NavBar = () => { 
  const user = useContext(SignContext)
  
  return (
    <>
  {user != null?
  <nav className="navbar navbar-expand-sm shadow-sm bg-body rounded fixed">
    <div className="container-fluid">
      <a className="navbar-brand" href="#"><img src="logo.svg" width={90} alt="log EA" /></a>
      <button className="navbar-toggler btn-sm" type="button" data-bs-toggle="collapse" data-bs-target="#collapsibleNavbar">
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse justify-content-end" id="collapsibleNavbar">
        <ul className="navbar-nav">        
          <li className="nav-item">
            < NavLink className="nav-link"  to={'/home'}>Olá, {user}</NavLink>
          </li>

          <li className="nav-item">
            <NavLink to={'/home'} className="nav-link">Home</NavLink>
          </li>
          {user != ''?
          <li className="nav-item">
            <NavLink to={'/usuarios'} className="nav-link">Usuarios</NavLink>
          </li>:''}

          <li className="nav-item"><NavLink to={'/'}><button type="button" className="btn btn-outline-warning btn-md">Sign-in</button></NavLink></li>
                
        </ul>
      </div>
    </div>
</nav>: <Login/>}
</>

  )
}

export default NavBar