import axios from "axios";
import { useState } from "react";
import TextField from '@mui/material/TextField';
import Button from "@mui/material/Button";
import LoadingButton from "@mui/lab/LoadingButton";
import Alert from "@mui/material/Alert";
import IconButton from "@mui/material/IconButton";
import CloseIcon from '@mui/icons-material/Close';


const FormUsers = () => {
    const [nomeUser, setNomeUser]= useState<string | null>(null)
    const [sobrenome, setSobrenome]= useState<string | null>(null)
    const [apelido, setApelido]= useState<string | null>(null)
    const [senhaUser, setSenhaUser]= useState<string | null>(null)
    const [email, setEmail]= useState<string | null>(null)
    const [loading, setLoading] = useState<boolean>(false) 
    const [erroMsg, setErroMsg] = useState<string | null>(null)
    const [sucesso, setSucesso] = useState<boolean>(false)

    const saveUser = async()=>{
      try{
        setLoading(true)
        const response = await axios.post('http://127.0.0.1:8000/api/v1/user/cadastrar/',{
          firstName:nomeUser, lastName:sobrenome,nickName:apelido, senhaUser:senhaUser, userEmail:email}).then(
                res=>res.data              
            )    
          .catch(error=> {
            console.error('Erro: ',error);
            setErroMsg(error.message);
            handleLimparCampos()
            });
             setLoading(false)
        
             if(response[1]=== 'Usuario cadastrado com sucesso'){
               setSucesso(true); 
               handleLimparCampos()                          
         }
        }

       catch(error){
        console.log(error)
      }
}
 
  function handleLimparCampos(){ 
     setNomeUser('')
     setSobrenome('')
     setApelido('')
     setSenhaUser('')
     setEmail('')
    }

  const handleSubmit = (e:any)=>{        
        e.preventDefault();        
        saveUser()                           
       }

       
  return (
    <>  
  
      {sucesso&&<div className="mb-3"><Alert severity="success"  action={
            <IconButton
              aria-label="close"
              color="inherit"
              size="small"
              onClick={() => {
              setSucesso(false) }}
            >
              <CloseIcon fontSize="inherit" />
            </IconButton>
          }>Usuario salvo com sucesso!</Alert></div>}
        {erroMsg&&<div className="mb-3"><Alert severity="error"
          action={
            <IconButton
              aria-label="close"
              color="inherit"
              size="small"
              onClick={() => {
              setErroMsg(null) }}
            >
              <CloseIcon fontSize="inherit" />
            </IconButton>
          }
        
        
        >{erroMsg}</Alert></div>           
      }
       
        <form onSubmit={handleSubmit} className="w-500">
           <Button
              sx={{
                position: 'absolute',
                right: 8,
                top: 8,
                }}
              />
          <div className="mb-2" >
            <TextField 
                required
                fullWidth
                label="Nome"
                onChange={(e)=>{setNomeUser(e.target.value)}}
                // sx={{width:{xd:100, sm:200, md:350}}}
                value={nomeUser}
        />
          </div>
          <div className="mb-2">
          <TextField
                required
                fullWidth
                label="Sobrenome"
                onChange={(e)=>{setSobrenome(e.target.value)}}
                // sx={{width:{xd:100, sm:200, md:350}}}
                value={sobrenome}
        />
          </div>
          <div className="mb-2">
          <TextField
                required
                label="Apelido"
                onChange={(e)=>{setApelido(e.target.value)}}
                fullWidth
                value={apelido}
        />
          </div>
          <div className="mb-3">
          <TextField
                required
                label="Senha"
                onChange={(e)=>{setSenhaUser(e.target.value)}}
                fullWidth
                value={senhaUser}
                type="password"
        />
          </div>
          <div className="mb-3">
          <TextField
                required
                label="Email"
                onChange={(e)=>{setEmail(e.target.value)}}
                fullWidth
                value={email}
                type="email"
        />
          </div>
          <div className="modal-footer">
           {loading?<LoadingButton loading loadingIndicator="Loading…" variant="outlined">
                    Fetch data
                </LoadingButton>
            :<Button variant="contained" color="success" size="medium" type="submit">
                  Enviar
                </Button>}

                
         </div>
        </form>

        

            
     
    </>  
    
    
  )
}

export default FormUsers